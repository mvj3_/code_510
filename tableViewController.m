self.tableViewController =[[TableViewController alloc] initWithNibName:@"TableViewController" bundle:nil];
self.viewController = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
 
self.tableViewController.title  =@"Table Usage";
self.viewController.title       =@"View Usage";
 
 
self.tabBarController= [[UITabBarController alloc] init];
 
NSMutableArray *controllers=[[NSMutableArray alloc] initWithCapacity:2];
[controllers addObject:self.tableViewController];
[controllers addObject:self.viewController];
 
 
self.tabBarController.viewControllers =controllers;
[self.window addSubview:self.tabBarController.view];